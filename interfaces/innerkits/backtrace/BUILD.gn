# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/hiviewdfx/faultloggerd/faultloggerd.gni")

backtrace_local_sources = [
  "backtrace_local.cpp",
  "backtrace_local_context.cpp",
  "backtrace_local_thread.cpp",
  "catchframe_local.cpp",
  "dfx_json_formatter.cpp",
]

if (defined(ohos_lite)) {
  shared_library("libbacktrace_local") {
    visibility = [ "*:*" ]

    include_dirs = [
      "include",
      "$c_utils_include_path",
      "$faultloggerd_common_path/dfxlog",
      "$faultloggerd_common_path/dfxutil",
      "$faultloggerd_frameworks_path/unwinder/include",
      "$faultloggerd_interfaces_path/common",
      "$faultloggerd_path/interfaces/innerkits/procinfo/include",
      "//third_party/libunwind/include",
      "//third_party/libunwind/src",
    ]

    if (target_cpu == "arm") {
      include_dirs += [ "//third_party/libunwind/include/tdep-arm" ]
    } else if (target_cpu == "arm64") {
      include_dirs += [ "//third_party/libunwind/include/tdep-aarch64" ]
    } else if (target_cpu == "x64" || target_cpu == "x86_64") {
      include_dirs += [ "//third_party/libunwind/include/tdep-x86_64" ]
    } else if (target_cpu == "mipsel") {
      include_dirs += [ "//third_party/libunwind/include/tdep-mips" ]
    }

    sources = backtrace_local_sources

    defines = [
      "HAVE_CONFIG_H",
      "HAVE_ELF_H",
      "HAVE_LINK_H",
      "is_ohos_lite",
    ]

    deps = [
      "$faultloggerd_common_path/dfxlog:dfx_hilog",
      "$faultloggerd_common_path/dfxutil:dfx_util",
      "$faultloggerd_frameworks_path/unwinder:dfx_unwinder_src",
      "$faultloggerd_interfaces_path/innerkits/procinfo:libdfx_procinfo",
      "//base/hiviewdfx/hilog_lite/frameworks/featured:hilog_shared",
      "//third_party/bounds_checking_function:libsec_shared",
      "//third_party/libunwind:libunwind",
    ]
  }

  static_library("backtrace_local") {
    # current static library is unuseful in ohos_lite type
  }
} else {
  config("dfx_backtrace_config") {
    visibility = [ "*:*" ]

    include_dirs = [
      "include",
      "$faultloggerd_common_path/dfxutil",
      "$faultloggerd_frameworks_path/unwinder/include",
      "$faultloggerd_interfaces_path/common",
      "$faultloggerd_interfaces_path/innerkits/procinfo/include",
    ]

    defines = [
      "is_ohos=${is_ohos}",
      "HAVE_CONFIG_H",
      "HAVE_ELF_H",
      "HAVE_LINK_H",
    ]
  }

  ohos_shared_library("libbacktrace_local") {
    public_configs = [
      ":dfx_backtrace_config",
      "$faultloggerd_common_path/build:coverage_flags",
    ]

    configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]

    sources = backtrace_local_sources

    deps = [
      "$faultloggerd_common_path/dfxlog:dfx_hilog",
      "$faultloggerd_common_path/dfxutil:dfx_util",
      "$faultloggerd_frameworks_path/unwinder:dfx_unwinder_src",
      "$faultloggerd_path/interfaces/innerkits/procinfo:libdfx_procinfo",
      "//third_party/bounds_checking_function:libsec_shared",
      "//third_party/jsoncpp:jsoncpp",
      "//third_party/libunwind:libunwind",
    ]
    include_dirs = [
      "$faultloggerd_common_path/dfxlog",
      "$faultloggerd_common_path/dfxutil",
      "$faultloggerd_frameworks_path/unwinder/include",
      "//third_party/libunwind/include",
      "//third_party/libunwind/include/tdep/",
      "//third_party/libunwind/src",
    ]
    version_script = "libbacktrace_local.map"
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
    ]

    install_images = [
      "system",
      "updater",
    ]

    output_extension = "so"
    innerapi_tags = [
      "chipsetsdk_indirect",
      "platformsdk_indirect",
    ]
    part_name = "faultloggerd"
    subsystem_name = "hiviewdfx"
  }

  ohos_static_library("backtrace_local") {
    public_configs = [ ":dfx_backtrace_config" ]

    configs = [ "//third_party/jsoncpp:jsoncpp_public_config" ]

    sources = backtrace_local_sources
    include_dirs = [
      "$faultloggerd_common_path/dfxlog",
      "$faultloggerd_common_path/dfxutil",
      "$faultloggerd_frameworks_path/unwind/include",
      "//third_party/libunwind/include",
      "//third_party/libunwind/include/tdep/",
      "//third_party/libunwind/src",
    ]

    deps = [
      "$faultloggerd_common_path/dfxlog:dfx_hilog_base",
      "$faultloggerd_frameworks_path/unwinder:dfx_unwinder_static_src",
      "$faultloggerd_interfaces_path/innerkits/procinfo:dfx_procinfo_static",
      "//third_party/bounds_checking_function:libsec_static",
      "//third_party/jsoncpp:jsoncpp_static",
      "//third_party/libunwind:libunwind_local",
    ]

    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
      "hilog:libhilog_base",
    ]

    part_name = "faultloggerd"
    subsystem_name = "hiviewdfx"
  }
}
